package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ActivityDisplay extends AppCompatActivity {

    TextView text;
    TextView text1;
    TextView text2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display);

        text = (TextView)findViewById(R.id.text);
        text1 = (TextView)findViewById(R.id.text1);
        text2 = (TextView)findViewById(R.id.text2);

        Intent intent = getIntent();
        Intent intent1 = getIntent();
        Intent intent2 = getIntent();
        String str = intent.getStringExtra("name");
        String str1 = intent1.getStringExtra("email");
        String str2 = intent2.getStringExtra("phone");
        text.setText(str);
        text1.setText(str1);
        text2.setText(str2);
    }
}