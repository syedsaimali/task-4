package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ActivityHome extends AppCompatActivity {

    EditText textBox;
    EditText textBox1;
    EditText textBox2;
    Button passButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textBox = (EditText)findViewById(R.id.textBox);
        textBox1 = (EditText)findViewById(R.id.textBox1);
        textBox2 = (EditText)findViewById(R.id.textBox2);
        passButton = (Button)findViewById(R.id.passButton);

        passButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = textBox.getText().toString();
                String str1 = textBox1.getText().toString();
                String str2 = textBox2.getText().toString();

                Intent intent = new Intent(getApplicationContext(), ActivityDisplay.class);
                intent.putExtra("name", str);
                intent.putExtra("email", str1);
                intent.putExtra("phone", str2);

                startActivity(intent);
            }
        });
    }
}